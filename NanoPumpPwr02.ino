/*
Program to set output to Solid State Relay (SSR) based on zero crossing detection (ZCT) circuit
ZCT generates an interrupt, then program sets pump on and off times based on value of A0 input.
Pump on and off times set pump power.
A0 is connected to an ESP32 for my project.  Interface to ESP32 through a webpage to set its pump power DAC outpu
ESP32 DAC output connects to A0 inside espresso machine

More info on HW for this circuit
https://sites.google.com/view/coffee4randy/projects/pump-control 

Runs on an old Arduino Nano for the project.
 
If using a Pot for A0 input
    center pin of the potentiometer to A0
    one side pin (either one) to ground
    the other side pin to +5V

Attach the ZCT pin to Arduino External Interrupt pin
Select the correct Interrupt # from the below table:
 Pin    |  Interrrupt # 
 d2      |  0            
 d3      |  1         

In the program pin d2 is used

Timing info for 60 or 50 Hz: 
  // 1 full 50Hz wave = 1/50 = 20ms    1/2 cycle = 10ms = 10000usecs
  // (10000us) / 128 = 78  
  // 1 full 60hz = 1/60 = 16.67 ms     1/2 cycle = 8.33ms = 8330 usecs
  // (8330us)/128 = 65
  //pump has an internal diode, and only uses power for 1/2 cycle (positive one)

  pump power limits
  started with full power, from 8330 to 0 usecs power on, powerlevel 128 to 0 where 0 is full power
  changed to limit powerlevel from 108 to 40  
  40*65 = 2600 usecs .    (8330-2600)/8330 = 69%    on for 8330-2600 = 5730 on time (max power)
  108*65 = 7020 usecs .   (8330-7020)/8330 = 16% .  on for 8330-7020 = 1310 on time (min power)
  
These are the limits because I tested with a spare ulka pump, and 16% is about where water flow stops
above 70%, there is no difference in the output waveform.  This makes sense because of the 2500 opto delay.
Above 70% (2600), the 2500 opto delay constant comes into effect, and there is no difference in the output.
The code for this is around line 100

low end could be higher, 25% would be 6248
  96*65 = 6240 usecs .   (8330-6240)/8330 = 25%   on for 8330-6240 = 2090
*/

#define BAUD 57600  // serial baud rate

// ************  Delay after stop  **************** 
// the time in usec to wait after zero-cross before switching off
// this eliminates the back-EMF from pump by waiting for current to drop before switching off
#define CONST_HALF_PERIOD 8333  //in usec 8333 for 60 hz, 10000 for 50 hz. 
#define DELAY_AFTER_STOP 4165  //1/4 cycle = 4165 for 60 Hz, 5000 for 50 hz
#define OPTO_DELAY 2500  //delay from zero crossing to optocoupler output changing state

// Define Pins and Variables
int InputPin = A0;    // select the analog input pin to use to set power level
const byte interruptPin = 2;     //interrupt input on D2
volatile int SSROutPin = 12;     // define output pin, connect this to SSR input
//volatile int ESPOutPin = 13;     // define output pin, connect this to ESP input to tell ESP that pump is on
int VAR_PowerLevel = 128;        // power level (0-128)  0 = full power, 128 = zero power
volatile int VAR_offtime = 0;    //time SSR is off, calculated in main loop
volatile int VAR_ontime = 0;     //time SSR is on,  calculated in main loop  
int VAR_SaveIn=0;                //save last value of A0 to decide if it changed.  Recalculate offtime and ontime if changed
volatile int VAR_savetime = 0;

void setup() {
  // declare SSR OUTPUT and init serial port:
  pinMode(SSROutPin, OUTPUT);

  Serial.begin(BAUD);
  
  //setup interrupt
  pinMode(interruptPin, INPUT);

  //enable interrupt on interrupt input pin.  Note that signal is falling when 60hz crosses zero going positive
  attachInterrupt(digitalPinToInterrupt(interruptPin), zero_crosss_int, FALLING);
}

//Interrupt routine where SSR is controlled
void zero_crosss_int()  
{
//if PowerLevel = 0 then full power.   
//  VAR_savetime = millis();           //save time for start of interrupt
  delayMicroseconds(VAR_offtime);    // ssr off time after zero crossing
  digitalWrite(SSROutPin, HIGH);   // turn on ssr
//  digitalWrite(SSROutPin, LOW);   // turn on ssr
  delayMicroseconds(VAR_ontime); // ssr on time
  digitalWrite(SSROutPin, LOW);    // turn off ssr and exit interrupt
  //digitalWrite(SSROutPin, HIGH);   // turn off ssr
  }

void loop() {
int VAR_aValue = 0;       // variable to store the value coming from the sensor

  // read the voltage at the input pin
  VAR_aValue = analogRead(InputPin);
  //calculate offtime and ontime if aValue changed
  if (VAR_SaveIn != VAR_aValue) {
    VAR_SaveIn = VAR_aValue;
  //VAR_aValue = A0 is 0-1023 for 5V input (pot), 0-720 for 3.2 input (DAC out of ESP32) 
  //powerlevel is scaled per comments in beginning, from full flow to no flow levels 
    VAR_PowerLevel = map(VAR_aValue, 0, 720, 108, 40);  //scale PowerLevel based on A0 input  Scales backwards because 0 is full power
    VAR_offtime = (65*VAR_PowerLevel) - OPTO_DELAY;    // For 60Hz (65*PowerLevel), For 50Hz (78*PowerLevel)  
    if (VAR_offtime < 0) {VAR_offtime = 0; }
  //set ssr on time to 1/2 cycle - offtime + the delay after stop - optocoupler depay
  //See comments above for explanation of delay after stop   
    VAR_ontime = (CONST_HALF_PERIOD - VAR_offtime + DELAY_AFTER_STOP - OPTO_DELAY);  
  }

  
// Serial.print(VAR_aValue);  
// Serial.print(", ");  
// Serial.print(VAR_PowerLevel);
// Serial.print(", ");  

  delay(500);
}

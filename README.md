# PumpPwr

Arduino project to control vibration pump power for espresso machine.  Requires SSR and optocoupler for Zero Cross Detection of AC waveform

Program sets output to Solid State Relay (SSR) based on zero crossing detection (ZCT) circuit
ZCT generates an interrupt, then program sets pump on and off times based on value of A0 input.
Pump on and off times set pump power.
Also has an output to signal that AC power is on.  Used to signal the main processor (ESP32) program that AC power to pump is on.

More info on the hardware design for this circuit and how to build it is at this link:
https://sites.google.com/view/coffee4randy/projects/hx-dumb-to-smart/pump-control-arduino

Compiled and tested on an old Arduino Nano with ATmega328p with old bootloader
Since it uses old bootloader, the included hex may not load on newer Nano's. 
 
If using a Pot for A0 input
    center pin of the potentiometer to A0
    one side pin (either one) to ground
    the other side pin to +5V

Attach the ZCT pin to Arduino External Interrupt pin

In the program pin d2 (interrupt 0) is used as the interrupt input pin.

Jan 2024:  Made NanoPumpPwr03 version.  Changed low end of pump power so pump turns completely off at 0 power for prefusion.  Created constants for setting low and high pump power in map command
